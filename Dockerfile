FROM node:18-alpine AS deps
RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json yarn.lock ./
RUN  yarn install --production


FROM node:18-alpine AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .
ENV NEXT_TELEMETRY_DISABLED 1
RUN yarn build
RUN mkdir -p ./.next/cache/images
RUN chown -R node:node .


FROM gcr.io/distroless/nodejs18-debian11 AS production
WORKDIR /app
USER nonroot:nonroot
ARG BACKOFFICE_URL
ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED=1
ENV BACKOFFICE_URL=${BACKOFFICE_URL}
ENV NEXT_SHARP_PATH=./node_modules/sharp
COPY --from=builder --chown=1001:1001 /app/.next/standalone ./
COPY --from=builder --chown=1001:1001 /app/public ./public
COPY --from=builder --chown=1001:1001 /app/.next/static ./.next/static
COPY --from=builder --chown=1001:1001 /app/.next/cache ./.next/cache
COPY --from=builder --chown=1001:1001 /app/.next/cache/images ./.next/cache/images

EXPOSE 3000
ENV PORT 3000
CMD ["server"]