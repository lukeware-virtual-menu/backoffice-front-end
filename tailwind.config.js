/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      black: "#000000",
      white: "#ffffff",
      slate: {
        50: "#f8fafc",
        100: "#f1f5f9",
        200: "#e2e8f0",
        300: "#cbd5e1",
        400: "#94a3b8",
        500: "#64748b",
        600: "#475569",
        700: "#334155",
        800: "#1e293b",
        900: "#0f172a",
        950: "#020617",
      },
      gray: {
        50: "#F9F9F9",
        100: "#F3F3F3",
        200: "#EAEAEA",
        300: "#DBDBDB",
        400: "#B7B7B7",
        500: "#989898",
        600: "#6F6F6F",
        700: "#5C5C5C",
        800: "#3D3D3D",
        900: "#1D1D1D",
      },
      primary: {
        50: "#F9EAE8",
        100: "#FBCFC0",
        200: "#F8B098",
        300: "#F7936F",
        400: "#F67C50",
        500: "#F56733",
        600: "#EA612F",
        700: "#DC5A2B",
        800: "#CE5328",
        900: "#B44922",
      },
    },
    container: {
      center: true,
    },
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [],
};
