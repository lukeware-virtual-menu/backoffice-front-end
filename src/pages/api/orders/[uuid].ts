import type { NextApiRequest, NextApiResponse } from "next";

const url = process.env.BACKOFFICE_URL;
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const uuid = req.query.uuid as string;
  let requestOptions: RequestInit;

  switch (req.method) {
    case "GET":
      requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      try {
        const response = await fetch(
          `${url}/orders/${uuid}`,
          requestOptions
        );
        const order = await response.json();
        res.status(200).json(order);
      } catch (error) {
        res.status(404).json({ message: "Order not found" });
      }
      break;
    case "PUT":
      requestOptions = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(req.body),
        redirect: "follow",
      };

      try {
        await fetch(
          `http://host.docker.internal:1323/api/v1/orders/${uuid}`,
          requestOptions
        );
        res.status(200).json({});
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Failed to update order" });
      }
      break;
  }
}
