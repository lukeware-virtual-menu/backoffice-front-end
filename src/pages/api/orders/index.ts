import { Order } from "@/types/order";
import type { NextApiRequest, NextApiResponse } from "next";

const url = process.env.BACKOFFICE_URL;
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Order[]>
) {
  const requestOptions: RequestInit = {
    method: "GET",
    redirect: "follow",
  };

  try {
    const response = await fetch(
      `${url}/orders?page=1&limit=1000`,
      requestOptions
    );
    const orders = await response.json();
    res.status(200).json(orders.data);
  } catch (error) {
    res.status(200).json([]);
  }
}