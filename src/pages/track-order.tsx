import Main from "@/components/Main";
import PlaceTableComponent from "@/components/PlaceTableComponent";
import WizardOrder from "@/components/WizardOrder";
import { OrderById, OrderUpdate } from "@/services/order-services";
import { Order } from "@/types/order";

import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const TrackOrder = () => {
  const router = useRouter();
  const { uuid, tableCode } = router.query;
  const [persent, setPercent] = useState(0);
  const [isFinished, setIsFinished] = useState(false);
  const [isInitiated, setIsInitiated] = useState(false);
  const [order, setOrder] = useState<Order>({
    createAt: new Date(),
    isFinished: false,
    isInitiated: false,
    items: [],
    subTotal: 0,
    tableCode: "",
    uuid: "",
    note: "",
  });

  useEffect(() => {
    const idOrder = typeof uuid === "string" ? uuid : "";
    OrderById(idOrder).then((order) => {
      setOrder(order);
    });
  }, [uuid]);

  const handleInitiated = () => {
    setIsInitiated(true);
    order.isInitiated = isInitiated;
    setOrder(order);
    OrderUpdate(order);
  };

  const handleGetPercent = (percent: number) => {
    setPercent(percent);
  };

  const handleIsFinished = (isFinished: boolean) => {
    setIsFinished(isFinished);
  };

  const handleSaveOrder = () => {
    order.isFinished = !order.isFinished;
    setOrder(order);
    OrderUpdate(order);
    alert("Order Finished success!");
    router.back();
  };

  return (
    <Main goBack={() => router.back()}>
      <div className="flex flex-col">
        <div className="flex justify-end my-2 space-x-2">
          <button
            onClick={handleInitiated}
            disabled={isInitiated}
            className={`bg-${isInitiated ? "gray-400" : "primary-700"} ${
              isInitiated
                ? "cursor-not-allowed"
                : "hover:bg-primary-800 active:bg-primary-900"
            } text-white h-10 w-52 rounded-md`}
          >
            Start Preparation
          </button>
          <button
            onClick={handleSaveOrder}
            disabled={!isFinished}
            className={`bg-${!isFinished ? "gray-400" : "primary-700"} ${
              !isFinished
                ? "cursor-not-allowed"
                : "hover:bg-primary-800 active:bg-primary-900"
            } text-white h-10 w-52 rounded-md`}
          >
            Confirm
          </button>
        </div>
        <WizardOrder
          id={typeof uuid === "string" ? uuid : ""}
          table={typeof tableCode === "string" ? tableCode : ""}
          percent={persent}
          handleIsFinished={handleIsFinished}
        />
        <PlaceTableComponent
          order={order}
          handleGetPercent={handleGetPercent}
        />
      </div>
    </Main>
  );
};

export default TrackOrder;
