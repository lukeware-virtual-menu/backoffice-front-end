import { Order } from "@/types/order";
import FilterComponent from "@/components/FilterComponent";
import OrderTableComponent from "@/components/OrderTableComponent";
import Main from "@/components/Main";
import { useEffect, useState } from "react";
import { formatterTimeString } from "@/utils/formatter-date";
import { OrdersAll } from "@/services/order-services";

export default function Home() {
  const [ordersFilter, setOrdersFilter] = useState<Order[]>([]);
  const [orders, setOrders] = useState<Order[]>([]);

  const handleOnClickClean = () => {
    setOrdersFilter(orders);
  };

  const handleOnClickSearch = (time: string, table: string) => {
    if (orders) {
      const ordersList = orders.filter(
        (it) =>
          formatterTimeString(it.createAt).replace(" am", "") === time ||
          it.tableCode === table
      );
      setOrdersFilter(ordersList);
    }
  };

  useEffect(() => {
    OrdersAll().then((orders) => {
      setOrdersFilter(orders);
      setOrders(orders);
    });
  });

  return (
    <Main>
      <FilterComponent
        title="Filter"
        handleOnClickSearch={handleOnClickSearch}
        handleOnClickClean={handleOnClickClean}
      />
      <OrderTableComponent orders={ordersFilter} />
    </Main>
  );
}
