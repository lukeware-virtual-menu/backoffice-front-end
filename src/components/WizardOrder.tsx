import { useEffect } from "react";

type Props = {
  id?: string;
  table?: string;
  percent: number;
  handleIsFinished: (isFinished: boolean) => void;
};

const WizardOrder = ({
  id,
  table,
  percent = 0,
  handleIsFinished = (isFinished: boolean) => {},
}: Props) => {
  useEffect(() => {
    if (percent == 100) {
      handleIsFinished(true);
      return;
    }
    handleIsFinished(false);
  }, [percent, handleIsFinished]);
  return (
    <>
      <div className="w-full bg-white rounded-md shadow-sm p-4">
        <div className="flex flex-col space-y-4">
          <div className="flex justify-between items-center mx-4">
            <span className="">
              <span className="text-gray-500">Order</span> {id} <span className="text-gray-500">from table</span> {table}
            </span>
            <span className="bg-primary-300 p-2 rounded-md text-white w-16 flex items-center justify-center">
              {Math.floor(percent)}%
            </span>
          </div>

          <div className="flex justify-center items-center">
            <div className="flex flex-col items-center justify-center w-[50rem] rounded-2xl lg:py-2 lg:mx-4">
              <span className="text-gray-900 text-xs lg:text-base">To do</span>
              <div className="lg:h-10 lg:w-10 bg-primary-800 text-white flex items-center justify-center rounded-full lg:m-1 shadow-sm">
                <i className="material-symbols-rounded ">check</i>
              </div>
            </div>

            {percent > 0 && (
              <div className="border-t border-primary-900 lg:w-full lg:block"></div>
            )}

            {percent <= 0 && (
              <div className="border-t border-gray-300 lg:w-full lg:block"></div>
            )}

            <div className="flex flex-col items-center justify-center w-[50rem] rounded-2xl lg:py-2 lg:mx-4">
              <span className=" text-gray-900 text-xs lg:text-base">
                In Preparation
              </span>
              {percent > 0 && (
                <div className="lg:h-10 lg:w-10 bg-primary-800 text-white flex items-center justify-center rounded-full lg:m-1 shadow-sm">
                  <i className="material-symbols-rounded">check</i>
                </div>
              )}

              {percent <= 0 && (
                <div className="lg:h-10 lg:w-10 bg-gray-400 text-white flex items-center justify-center rounded-full lg:m-1 shadow-sm">
                  <i className="material-symbols-rounded">exclamation</i>
                </div>
              )}
            </div>

            {percent === 100 && (
              <div className="border-t border-primary-900 lg:w-full lg:block"></div>
            )}

            {percent < 100 && (
              <div className="border-t border-gray-300 lg:w-full lg:block"></div>
            )}

            <div className="flex flex-col items-center justify-center w-[50rem] rounded-2xl lg:py-2 lg:mx-4">
              <span className="text-gray-900 text-xs lg:text-base">
                Finished
              </span>
              {percent === 100 && (
                <div className="lg:h-10 lg:w-10 bg-primary-800 text-white flex items-center justify-center rounded-full lg:m-1 shadow-sm">
                  <i className="material-symbols-rounded">check</i>
                </div>
              )}

              {percent < 100 && (
                <div className="lg:h-10 lg:w-10 bg-gray-400 text-white flex items-center justify-center rounded-full lg:m-1 shadow-sm">
                  <i className="material-symbols-rounded">exclamation</i>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default WizardOrder;
