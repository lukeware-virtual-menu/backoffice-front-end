import { OrderUpdate } from "@/services/order-services";
import { Item } from "@/types/Items";
import { Order } from "@/types/order";

import { useEffect, useState } from "react";

type Props = {
  order: Order;
  handleGetPercent: (percent: number) => void;
};

const PlaceTableComponent = ({ order, handleGetPercent }: Props) => {
  const [checkedItems, setCheckedItems] = useState<Item[]>([]);
  const [orderSelected, setOrderSelected] = useState<Order>({
    createAt: new Date(),
    isFinished: false,
    isInitiated: false,
    items: [],
    subTotal: 0,
    tableCode: "",
    uuid: "",
    note: "",
  });

  const handleCheck = (id: number) => {
    const itens = checkedItems.map((item) =>
      item.sequence === id ? { ...item, check: !item.check } : item
    );
    orderSelected.items = itens;
    setCheckedItems(itens);
    setOrderSelected(orderSelected);
    OrderUpdate(orderSelected).then();
  };

  useEffect(() => {
    if (checkedItems) {
      const length = checkedItems.length;
      const checked = checkedItems.filter((it) => it.check === true).length;
      handleGetPercent((checked * 100) / length);
    }
  }, [checkedItems, handleGetPercent]);

  useEffect(() => {
    setCheckedItems(order.items);
    setOrderSelected(order);
  }, [order]);

  return (
    <div className="max-h-[37rem] overflow-auto bg-white shadow-sm rounded-md p-4 my-2">
      <table className="table-auto w-full text-sm text-center">
        <thead>
          <tr className="text-gray-900 flex flex-row items-center">
            <th className="flex-1">ID</th>
            <th className="flex-1">Name</th>
            <th className="flex-1 lg:block hidden">House Specialty</th>
            <th className="flex-1">Quantity</th>
            <th className="flex-1  lg:block hidden">Note</th>
            <th className="flex-1">Finished</th>
          </tr>
        </thead>
        <tbody className=" text-gray-800">
          {order.items &&
            checkedItems &&
            checkedItems.map((item) => (
              <tr
                key={item.sequence}
                className="border-b border-gray-300 flex flex-row items-center"
              >
                <td className="flex-1 my-2 text-justify flex justify-center">
                  #{item.sequence}
                </td>
                <td className="flex-1 my-2 text-justify flex justify-center">
                  {item.title}
                </td>
                <td className="flex-1 my-2 text-justify lg:flex justify-center lg:block hidden">
                  {item.houseSpecialty.toString()}
                </td>
                <td className="flex-1 my-2 text-justify flex justify-center">
                  {item.quantity}
                </td>
                <td className="flex-1 my-2 text-justify lg:flex justify-center lg:block hidden">
                  {item.note}
                </td>
                <td className="flex-1 my-2">
                  <button
                    className="h-10 w-14"
                    onClick={() => handleCheck(item.sequence)}
                  >
                    {!item.check && (
                      <span className="material-symbols-rounded text-gray-700 flex justify-center items-center">
                        check_box_outline_blank
                      </span>
                    )}
                    {item.check && (
                      <span className="material-symbols-rounded text-primary-800 flex justify-center items-center">
                        select_check_box
                      </span>
                    )}
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default PlaceTableComponent;
