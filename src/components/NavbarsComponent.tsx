import { useAppContext } from "@/hook/UseAppContext";
import Image from "next/image";
import { useEffect, useState } from "react";

type Props = {
  goBack?: () => void;
};

const NavbarsComponent = ({ goBack = undefined }: Props) => {
  const [isNavFixed, setIsNavFixed] = useState(false);

  const navClass = isNavFixed ? "fixed top-0 left-0 right-0" : "";
  const goBackClass = isNavFixed ? "top-3" : "";

  useEffect(() => {
    const handleScroll = () => {
      if (window.pageYOffset > 0) {
        setIsNavFixed(true);
      } else {
        setIsNavFixed(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      <div
        className={`w-full flex justify-center items-center bg-white px-4 shadow-md z-10 ${navClass}`}
      >
        {goBack && (
          <>
            <button
              className={`fixed right-0 left-0 ${goBackClass} ml-4 h-10 w-10 rounded-md shadow-md text-white bg-primary-700 hover:bg-primary-800 active:bg-primary-900 lg:h-10 lg:w-10 flex items-center justify-center`}
              onClick={goBack}
            >
              <i className="material-symbols-rounded">arrow_back</i>
            </button>
          </>
        )}
        <Image
          alt="logo"
          src={"/logo.png"}
          width={250}
          height={100}
          priority={true}
          className="w-32"
        />
      </div>
    </>
  );
};

export default NavbarsComponent;
