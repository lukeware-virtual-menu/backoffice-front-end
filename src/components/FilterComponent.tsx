import { useState } from "react";
import DatePicker from "./DatePicker";

type Props = {
  title: string;
  handleOnClickSearch: (date: string, table: string) => void;
  handleOnClickClean: () => void;
};

const FilterComponent = ({
  title,
  handleOnClickSearch,
  handleOnClickClean,
}: Props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [date, setDate] = useState("");
  const [table, setTable] = useState("");

  const handleDateChange = (e: any) => {
    setDate(e);
  };

  const handleTableChange = (e: any) => {
    setTable(e.target.value);
  };

  return (
    <div className="border border-gray-300 rounded-md w-full my-2 bg-white shadow-sm">
      <div
        className="flex items-center justify-between p-4 cursor-pointer z-50"
        onClick={() => setIsOpen(!isOpen)}
      >
        <h2 className="text-lg font-medium">{title}</h2>
        <i
          className={`material-symbols-rounded transform transition-transform duration-300 ${
            isOpen ? "rotate-90" : ""
          }`}
        >
          chevron_right
        </i>
      </div>

      <div
        className={`${
          isOpen ? "h-auto " : "h-0"
        } transform transition-transform  overflow-hidden`}
      >
        <div className="px-4 pb-4 lg:pb-0 border-t border-gray-300 w-full flex flex-col lg:flex-row lg:items-end lg:space-x-4 space-y-4">
          <DatePicker handleDateChange={handleDateChange} />
          <div className="flex flex-col justify-center flex-1">
            <label className="text-gray-700 flex-1 my-1">Table Number</label>
            <input
              title="table"
              type="text"
              placeholder="Table Number"
              onChange={handleTableChange}
              className="flex-1 w-full text-sm bg-gray-100 px-4 py-2 rounded-md text-gray-500 focus:bg-gray-50 placeholder-gray-400 border focus:outline-none focus:border-primary-800 focus:border-1"
            />
          </div>
        </div>

        <div className="px-4 pb-4 w-full flex flex-col justify-end lg:flex-row lg:items-end lg:space-x-2 space-y-4">
          <button
            title="Order"
            className="bg-gray-300 w-full lg:w-32 h-10 rounded-md hover:bg-gray-400 active:bg-gray-500"
            onClick={handleOnClickClean}
          >
            <span className="text-white">Clean</span>
          </button>
          <button
            title="Order"
            className="bg-primary-700  w-full lg:w-32 h-10 rounded-md hover:bg-primary-800 active:bg-primary-900"
            onClick={() => {
              handleOnClickSearch(date, table);
            }}
          >
            <span className="text-white">Search</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default FilterComponent;
