import { useState } from "react";
import { formatterEnUSmin } from "@/utils/formatter-date";

type Props = {
  handleDateChange: (value: string) => void;
};

const DatePicker = ({ handleDateChange }: Props) => {
  const [selectedDate, setSelectedDate] = useState("");
  return (
    <div className="lg:flex-1">
      <div className="flex flex-col justify-center">
        <label className="text-gray-700 flex-1 my-1">Order Date</label>
        <input
          title="date"
          type="time"
          value={selectedDate}
          min={`${formatterEnUSmin}`}
          onChange={(e: any) => {
            setSelectedDate(e.target.value);
            handleDateChange(e.target.value);
          }}
          className="flex-1 w-full text-sm bg-gray-100 h-9 px-4 py-2 rounded-md text-gray-500 focus:bg-gray-50 placeholder-gray-400 border focus:outline-none focus:border-primary-800 focus:border-1"
        />
      </div>
    </div>
  );
};

export default DatePicker;
