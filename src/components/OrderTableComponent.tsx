import { Order } from "@/types/order";
import { formatterTimeString } from "@/utils/formatter-date";
import { sortByDate } from "@/utils/sort-by-date";
import { useRouter } from "next/router";

type Props = {
  orders: Order[];
};

const OrderTableComponent = ({ orders }: Props) => {
  const router = useRouter();

  const handleClick = (order: Order) => {
    router.push({
      pathname: "/track-order",
      query: { uuid: order.uuid, tableCode: order.tableCode },
    });
  };

  return (
    <div className="max-h-[37rem] overflow-auto bg-white shadow-sm rounded-md p-4">
      <table className="table-auto w-full text-sm text-center">
        <thead>
          <tr className="text-gray-900 flex flex-row items-center">
            <th className="flex-1">ID</th>
            <th className="flex-1">Table</th>
            <th className="flex-1">Order Date</th>
            <th className="flex-1">Action</th>
          </tr>
        </thead>
        <tbody className=" text-gray-800">
          {orders &&
            sortByDate(orders).map((order) => (
              <tr
                key={order.uuid}
                className="border-b border-gray-300 flex flex-row items-center"
              >
                <td className="flex-1 my-2 text-justify flex justify-center">
                  #{order.uuid}
                </td>
                <td className="flex-1 my-2 text-justify flex justify-center">
                  {order.tableCode}
                </td>
                <td className="flex-1 my-2 text-justify flex justify-center">
                  {formatterTimeString(order.createAt)}
                </td>
                <td className="flex-1 my-2 text-justify flex justify-center">
                  <button
                    className="text-primary-800 bg-primary-700 focus:bg-primary-800 active:bg-primary-900 h-10 w-10 rounded-md shadow-md flex justify-center items-center text-center"
                    onClick={() => handleClick(order)}
                  >
                    <i className="material-symbols-rounded text-white flex justify-center items-center text-center">
                      checklist
                    </i>
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default OrderTableComponent;
