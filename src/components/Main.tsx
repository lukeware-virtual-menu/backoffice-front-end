import { Lexend } from "next/font/google";
import NavbarsComponent from "./NavbarsComponent";

const inter = Lexend({
  subsets: ["latin"],
  style: ["normal"],
  preload: true,
});

type Props = {
  goBack?: () => void;
  children: any;
};

const Main = ({ children, goBack }: Props) => {
  return (
    <div className={`${inter.className}`}>
      <NavbarsComponent goBack={goBack} />
      <main className={`flex flex-col items-center justify-start mx-4 pb-16`}>
        <div className="container">{children}</div>
      </main>
    </div>
  );
};

export default Main;
