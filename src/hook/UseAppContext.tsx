import { createContext, useContext, useState } from "react";

interface AppContextProps {
  quantityTotal: number;
  setQuantityTotal: (quantityTotal: number) => void;
}

export const AppContext = createContext<AppContextProps>({
  quantityTotal: 0,
  setQuantityTotal: () => {},
});

export const useAppContext = () => useContext(AppContext);

const AppProvider = ({ children }: any) => {
  const [quantityTotal, setQuantityTotal] = useState(0);

  return (
    <AppContext.Provider
      value={{
        quantityTotal,
        setQuantityTotal,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppProvider;
