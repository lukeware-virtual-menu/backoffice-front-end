export type Item = {
  sequence: number;
  houseSpecialty: boolean;
  title: string;
  description: string;
  price: number;
  category: string;
  quantity: number;
  note?: string;
  check: boolean;
};
