import { Item } from "./Items";

export type Order = {
  uuid: string;
  tableCode: string;
  createAt: Date;
  items: Item[];
  subTotal: number;
  note?: string;
  isFinished: boolean;
  isInitiated: boolean;
};
