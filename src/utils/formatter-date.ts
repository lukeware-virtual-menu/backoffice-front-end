export const formatterEnUSmin =
  "${new Date(creatAt).toLocaleTimeString('en-AU', {year: '2-digit',month: 'long',day: '2-digit',hour: '2-digit',minute: '2-digit',hour12: true,timeZone: 'Australia/Sydney'})}";

export const formatterDateTime = (creatAt: any): string => {
  return new Date(creatAt).toLocaleTimeString("en-AU", {
    year: "2-digit",
    month: "long",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
    hour12: true,
    timeZone: "Australia/Sydney",
  });
};

export const formatterTimeString = (creatAt: any): string => {
  const date = new Date(creatAt);
  return date.toLocaleTimeString("en-AU", {
    hour: "2-digit",
    minute: "2-digit",
    hour12: true,
    timeZone: "Australia/Sydney",
  });
};
