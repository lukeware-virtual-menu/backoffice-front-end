import { Order } from "@/types/order";
import { stringify } from "postcss";

export const sortByDate = (orders: Order[]) => {
  return orders.sort((a, b) => {
    if (typeof b.createAt === "string") {
      const dateB = new Date(b.createAt);
      const dateA = new Date(a.createAt);
      return  dateA.getTime() - dateB.getTime();
    }
    return b.createAt.getTime() - a.createAt.getTime();
  });
};
