import { Order } from "@/types/order";

export const OrdersAll = async (): Promise<Order[]> => {
  try {
    const res = await fetch(`/api/orders`);
    return (await res.json()) as Order[];
  } catch (error) {
    console.error(error);
  }
  return [] as Order[];
};

export const OrderById = async (uuid: string): Promise<Order> => {
  if (uuid) {
    try {
      const res = await fetch(`/api/orders/${uuid}`);
      return (await res.json()) as Order;
    } catch (error) {
      console.error(error);
    }
  }
  return {} as Order;
};

export const OrderUpdate = async (order: Order): Promise<void> => {
  if (order) {
    try {
      const response = await fetch(`/api/orders/${order.uuid}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(order),
      });
      if (!response.ok) {
        throw new Error("Failed to update order");
      }
    } catch (error) {
      console.error(error);
    }
  }
};
