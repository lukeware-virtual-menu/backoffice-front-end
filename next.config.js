/** @type {import('next').NextConfig} */
const nextConfig = {
  output: "standalone",
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: "http://127.0.0.1:3000/api/:path*",
        has: [{ type: "query", key: "overrideMe" }],
      },
    ];
  },
  reactStrictMode: true,
};

module.exports = nextConfig;
